use minijinja::{context, Environment};
use serde::Serialize;

use crate::events::{Event, EventKind};

#[derive(Clone, Serialize)]
pub struct Question {
    pub id: String,
}

pub struct Questions {
    questions: Vec<Question>,
}

impl Questions {
    pub fn new() -> Self {
        Questions { questions: vec![] }
    }

    pub fn handle_event(self: &mut Self, event: &Event) {
        match &event.kind {
            EventKind::QuestionCreated(data) => {
                let question = Question {
                    id: data.id.clone(),
                };
                self.questions.push(question);
            }
            EventKind::QuestionDeleted(data) => {
                for (i, question) in self.questions.iter().enumerate() {
                    if &question.id == &data.id {
                        self.questions.remove(i);
                        break;
                    }
                }
            }
            _ => {}
        }
    }

    pub fn render_all(&self, tpl_env: &Environment) -> String {
        let tpl = tpl_env.get_template("questions.j2").unwrap();
        tpl.render(context! {questions => self.questions}).unwrap()
    }

    pub fn get_all_owned(&self) -> Vec<Question> {
        self.questions.clone()
    }
}
