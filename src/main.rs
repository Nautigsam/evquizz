mod app;
mod events;
mod questions;
mod quizzes;
mod router;

use std::sync::Arc;

use axum::response::sse;
use events::{Event, Events};
use futures_util::TryFutureExt;
use minijinja::{path_loader, Environment};
use quizzes::Quizzes;
use sqlx::{migrate::MigrateDatabase, sqlite::SqlitePoolOptions, Sqlite};
use tokio::{
    self,
    sync::{broadcast, mpsc, RwLock},
};

use crate::{questions::Questions, router::Router};

#[derive(Debug)]
enum Error {
    Server,
    EventLoop,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let db_url = "sqlite:data.db";
    if !Sqlite::database_exists(db_url).await.unwrap() {
        Sqlite::create_database(db_url).await.unwrap();
    }
    let pool = SqlitePoolOptions::new().connect(db_url).await.unwrap();

    // Aggregates
    let questions = Arc::new(RwLock::new(Questions::new()));
    let quizzes = Arc::new(RwLock::new(Quizzes::new(questions.clone())));

    let (db_sender, mut db_receiver) = mpsc::unbounded_channel::<Event>();
    let (client_sender, _) = broadcast::channel::<sse::Event>(42);

    let quizzes_for_db = quizzes.clone();
    let questions_for_db = questions.clone();
    let client_sender_for_db = client_sender.clone();
    let event_loop = tokio::task::spawn(async move {
        loop {
            let event = match db_receiver.recv().await {
                None => return,
                Some(event) => event,
            };
            questions_for_db.write().await.handle_event(&event);
            quizzes_for_db.write().await.handle_event(&event).await;
            client_sender_for_db.send(event.clone().into()).unwrap_or(0);
            println!("{}", event);
        }
    });

    let events = Events::new(db_sender);
    println!("Replaying events from store...");
    events.initialize(&pool).await;
    println!("Done");

    let mut tpl_env = Environment::new();
    tpl_env.set_loader(path_loader("src/templates"));

    let router = Router::new(
        Arc::new(tpl_env),
        pool,
        client_sender,
        events,
        quizzes,
        questions,
    );

    let address = "127.0.0.1:3000";
    let server = axum::Server::bind(&address.parse().unwrap()).serve(router.into_make_service());

    println!("Listening at http://{}", address);

    let res = tokio::try_join!(
        server.map_err(|_| Error::Server),
        event_loop.map_err(|_| Error::EventLoop)
    );
    res.map(|_| ())
}
