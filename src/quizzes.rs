use std::sync::Arc;

use minijinja::{context, Environment};
use serde::Serialize;
use tokio::sync::RwLock;

use crate::{
    events::{Event, EventKind},
    questions::{Question, Questions},
};

#[derive(Serialize)]
pub struct Quizz {
    pub id: String,
    pub questions: Vec<Question>,
}

pub struct Quizzes {
    quizzes: Vec<Quizz>,
    questions: Arc<RwLock<Questions>>,
}

impl Quizzes {
    pub fn new(questions: Arc<RwLock<Questions>>) -> Self {
        Quizzes {
            quizzes: vec![],
            questions,
        }
    }

    pub async fn handle_event(&mut self, event: &Event) {
        match &event.kind {
            EventKind::QuizzCreated(data) => {
                let quizz = Quizz {
                    id: data.id.clone(),
                    questions: self.questions.read().await.get_all_owned(),
                };
                self.quizzes.push(quizz);
            }
            EventKind::QuizzDeleted(data) => {
                for (i, quizz) in self.quizzes.iter().enumerate() {
                    if &quizz.id == &data.id {
                        self.quizzes.remove(i);
                        break;
                    }
                }
            }
            _ => {}
        }
    }

    pub fn render_all(self: &Self, tpl_env: &Environment) -> String {
        let tpl = tpl_env.get_template("quizzes.j2").unwrap();
        tpl.render(context! {quizzes => self.quizzes}).unwrap()
    }
}
