use minijinja::{context, Environment};

pub struct App;

impl App {
    pub fn render(tpl_env: &Environment) -> String {
        let tpl = tpl_env.get_template("app.j2").unwrap();
        tpl.render(context! {}).unwrap()
    }
}
