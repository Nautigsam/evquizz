use std::{convert::Infallible, sync::Arc};

use axum::{
    extract::{Path, State},
    http::header::CONTENT_TYPE,
    response::{
        sse::{self, KeepAlive},
        AppendHeaders, IntoResponse, Sse,
    },
    routing::{get, post, IntoMakeService},
};
use futures_util::Stream;
use minijinja::Environment;
use sqlx::{Pool, Sqlite};
use tokio::sync::{broadcast::Sender, RwLock};
use tokio_stream::{
    wrappers::{errors::BroadcastStreamRecvError, BroadcastStream},
    StreamExt,
};

use crate::{
    app::App,
    events::{
        Event, EventKind, Events, QuestionCreatedData, QuestionDeletedData, QuizzCreatedData,
        QuizzDeletedData,
    },
    questions::Questions,
    quizzes::Quizzes,
};

#[derive(Clone)]
struct RouterState {
    /// This is crucial not to clone Environment to keep templates loaded
    tpl_env: Arc<Environment<'static>>,
    pool: Pool<Sqlite>,
    event_sender: Sender<sse::Event>,
    events: Events,
    quizzes: Arc<RwLock<Quizzes>>,
    questions: Arc<RwLock<Questions>>,
}

impl RouterState {
    async fn dispatch(&self, event: Event) {
        self.events.add(&self.pool, event).await
    }
}

pub struct Router {
    state: RouterState,
}

impl Router {
    pub fn new(
        tpl_env: Arc<Environment<'static>>,
        pool: Pool<Sqlite>,
        event_sender: Sender<sse::Event>,
        events: Events,
        quizzes: Arc<RwLock<Quizzes>>,
        questions: Arc<RwLock<Questions>>,
    ) -> Self {
        let state = RouterState {
            tpl_env,
            pool,
            event_sender,
            events,
            quizzes,
            questions,
        };
        Self { state }
    }

    pub fn into_make_service(self) -> IntoMakeService<axum::routing::Router> {
        let inner_router = axum::routing::Router::new()
            .route("/", get(get_root))
            .route("/sse", get(get_sse))
            .route("/h/quizzes", get(get_quizzes))
            .route("/h/create-quizz", post(create_quizz))
            .route("/h/delete-quizz/:id", post(delete_quizz))
            .route("/h/questions", get(get_questions))
            .route("/h/create-question", post(create_question))
            .route("/h/delete-question/:id", post(delete_question))
            .with_state(self.state);
        inner_router.into_make_service()
    }
}

async fn get_root(State(state): State<RouterState>) -> impl IntoResponse {
    respond_html(App::render(&state.tpl_env))
}

async fn get_sse(
    State(state): State<RouterState>,
) -> Sse<impl Stream<Item = Result<sse::Event, Infallible>>> {
    let receiver = state.event_sender.subscribe();
    let stream = BroadcastStream::new(receiver).map(|el| match el {
        Ok(event) => Ok(event),
        Err(BroadcastStreamRecvError::Lagged(count)) => Ok(sse::Event::default()
            .event("lagged")
            .data(count.to_string())),
    });
    Sse::new(stream).keep_alive(KeepAlive::default())
}

async fn get_quizzes(State(state): State<RouterState>) -> impl IntoResponse {
    respond_html(state.quizzes.read().await.render_all(&state.tpl_env))
}

async fn create_quizz(State(state): State<RouterState>) -> impl IntoResponse {
    state
        .dispatch(Event::new(EventKind::QuizzCreated(QuizzCreatedData::new())))
        .await;
}

async fn delete_quizz(
    State(state): State<RouterState>,
    Path(quizz_id): Path<String>,
) -> impl IntoResponse {
    state
        .dispatch(Event::new(EventKind::QuizzDeleted(QuizzDeletedData {
            id: quizz_id,
        })))
        .await;
}

async fn get_questions(State(state): State<RouterState>) -> impl IntoResponse {
    respond_html(state.questions.read().await.render_all(&state.tpl_env))
}

async fn create_question(State(state): State<RouterState>) -> impl IntoResponse {
    state
        .dispatch(Event::new(EventKind::QuestionCreated(
            QuestionCreatedData::new(),
        )))
        .await;
}

async fn delete_question(
    State(state): State<RouterState>,
    Path(question_id): Path<String>,
) -> impl IntoResponse {
    state
        .dispatch(Event::new(EventKind::QuestionDeleted(
            QuestionDeletedData { id: question_id },
        )))
        .await;
}

fn respond_html(body: String) -> impl IntoResponse {
    (AppendHeaders([(CONTENT_TYPE, "text/html")]), body)
}
