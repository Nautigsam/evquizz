use axum::response::sse;
use serde::{Deserialize, Serialize};
use sqlx::{sqlite::SqliteRow, FromRow, Pool, QueryBuilder, Row, Sqlite};
use tokio::sync::mpsc::UnboundedSender;
use tokio_stream::StreamExt;
use ulid::Ulid;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct QuizzCreatedData {
    pub id: String,
}

impl QuizzCreatedData {
    pub fn new() -> Self {
        Self {
            id: Ulid::new().to_string(),
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct QuizzDeletedData {
    pub id: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct QuestionCreatedData {
    pub id: String,
}

impl QuestionCreatedData {
    pub fn new() -> Self {
        Self {
            id: Ulid::new().to_string(),
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct QuestionDeletedData {
    pub id: String,
}

#[derive(Clone, Debug)]
pub enum EventKind {
    QuizzCreated(QuizzCreatedData),
    QuizzDeleted(QuizzDeletedData),
    QuestionCreated(QuestionCreatedData),
    QuestionDeleted(QuestionDeletedData),
}

impl EventKind {
    fn serialize<T>(data: T) -> serde_json::Value
    where
        T: Serialize,
    {
        serde_json::to_value(data).unwrap_or_default()
    }
}

impl From<EventKind> for String {
    fn from(val: EventKind) -> Self {
        match val {
            EventKind::QuizzCreated(_) => "quizzcreated".to_string(),
            EventKind::QuizzDeleted(_) => "quizzdeleted".to_string(),
            EventKind::QuestionCreated(_) => "questioncreated".to_string(),
            EventKind::QuestionDeleted(_) => "questiondeleted".to_string(),
        }
    }
}
impl<'a> From<&'a EventKind> for String {
    fn from(val: &'a EventKind) -> Self {
        match *val {
            EventKind::QuizzCreated(_) => "quizzcreated".to_string(),
            EventKind::QuizzDeleted(_) => "quizzdeleted".to_string(),
            EventKind::QuestionCreated(_) => "questioncreated".to_string(),
            EventKind::QuestionDeleted(_) => "questiondeleted".to_string(),
        }
    }
}

impl From<EventKind> for serde_json::Value {
    fn from(val: EventKind) -> Self {
        match val {
            EventKind::QuizzCreated(data) => EventKind::serialize(data),
            EventKind::QuizzDeleted(data) => EventKind::serialize(data),
            EventKind::QuestionCreated(data) => EventKind::serialize(data),
            EventKind::QuestionDeleted(data) => EventKind::serialize(data),
        }
    }
}

#[derive(Debug)]
struct UnknownDbEvent {}
impl std::fmt::Display for UnknownDbEvent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("Unknown event from database")
    }
}
impl std::error::Error for UnknownDbEvent {}

#[derive(Clone, Debug)]
pub struct Event {
    pub id: String,
    pub kind: EventKind,
}

impl Event {
    pub fn new(kind: EventKind) -> Self {
        Self {
            id: Ulid::new().to_string(),
            kind,
        }
    }
}

impl std::fmt::Display for Event {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "{}({}) {{{:?}}}",
            Into::<String>::into(&self.kind),
            self.id,
            self.kind
        ))
    }
}

impl From<Event> for sse::Event {
    fn from(event: Event) -> Self {
        Self::default()
            .event(Into::<String>::into(event.kind))
            .data(event.id)
    }
}

impl FromRow<'_, SqliteRow> for Event {
    fn from_row(row: &'_ SqliteRow) -> Result<Self, sqlx::Error> {
        let kind_col = row.try_get::<&str, &str>("kind")?;
        let data_col = row.try_get::<sqlx::types::JsonValue, &str>("data")?;
        let kind = match kind_col {
            "quizzcreated" => Ok(EventKind::QuizzCreated(
                serde_json::from_value(data_col).map_err(|err| {
                    sqlx::error::Error::ColumnDecode {
                        index: "data".to_string(),
                        source: Box::new(err),
                    }
                })?,
            )),
            "quizzdeleted" => Ok(EventKind::QuizzDeleted(
                serde_json::from_value(data_col).map_err(|err| {
                    sqlx::error::Error::ColumnDecode {
                        index: "data".to_string(),
                        source: Box::new(err),
                    }
                })?,
            )),
            "questioncreated" => Ok(EventKind::QuestionCreated(
                serde_json::from_value(data_col).map_err(|err| {
                    sqlx::error::Error::ColumnDecode {
                        index: "data".to_string(),
                        source: Box::new(err),
                    }
                })?,
            )),
            "questiondeleted" => Ok(EventKind::QuestionDeleted(
                serde_json::from_value(data_col).map_err(|err| {
                    sqlx::error::Error::ColumnDecode {
                        index: "data".to_string(),
                        source: Box::new(err),
                    }
                })?,
            )),
            _ => Err(sqlx::error::Error::ColumnDecode {
                index: "kind".to_string(),
                source: Box::new(UnknownDbEvent {}),
            }),
        }?;
        Ok(Self {
            id: row.try_get("id")?,
            kind,
        })
    }
}

#[derive(Clone)]
pub struct Events {
    sender: UnboundedSender<Event>,
}

impl Events {
    pub fn new(sender: UnboundedSender<Event>) -> Self {
        Self { sender }
    }

    pub async fn initialize(&self, pool: &Pool<Sqlite>) {
        sqlx::query(
            "
        create table if not exists events (
            id text collate nocase primary key not null,
            kind text not null,
            data json not null
        );
    ",
        )
        .execute(pool)
        .await
        .unwrap();

        let mut stream =
            sqlx::query_as::<Sqlite, Event>("select * from events order by id asc;").fetch(pool);
        while let Some(event) = stream.next().await {
            self.sender.send(event.unwrap()).unwrap();
        }
    }

    pub async fn add(&self, pool: &Pool<Sqlite>, event: Event) {
        let mut query_builder: QueryBuilder<Sqlite> =
            QueryBuilder::new("insert into events (id,kind,data) ");
        query_builder.push_values([event.clone()], |mut b, event| {
            b.push_bind(event.id.clone())
                .push_bind::<String>(event.kind.clone().into())
                .push_bind::<serde_json::Value>(event.kind.into());
        });
        let query = query_builder.build();
        query.execute(pool).await.unwrap();
        self.sender.send(event).unwrap();
    }
}
